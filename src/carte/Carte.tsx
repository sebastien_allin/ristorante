import HeadBand from "../components/HeadBand";
import LeftSide from "../components/LeftSide";
import RightSide from "../components/RightSide";

export default function Carte() {
  return (
    <div>
      <HeadBand
        url="https://roma-montargis.com/wp-content/uploads/2021/03/diapo-menu-carte.jpg?id=12728"
        firstText=""
        secondText="LA CARTE"
      />
      <div className="flex">
        <LeftSide
          url={
            "https://roma-montargis.com/wp-content/uploads/2023/06/roma2-1-768x1064.jpg"
          }
          title={"Découvrez Notre Carte"}
          span={"carte du restaurant"}
          height=""
        />
        <RightSide
          images={[
            {
              url: "https:roma-montargis.com/wp-content/uploads/2021/03/carte-roma-montargis-212x300.jpg",
              heigth: "510px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/CARTE-RESTO-212x300.jpg",
              heigth: "510px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/03/pizza-montargis-45-193x300.png",
              heigth: "510px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/06/salle-resto-italien-roma-montargis.jpg",
              heigth: "400px",
            },
          ]}
        />
      </div>
    </div>
  );
}
