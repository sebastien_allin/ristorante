import HeadBand from "../components/HeadBand";
import LeftSide from "../components/LeftSide";
import RightSide from "../components/RightSide";

export default function Menu() {
  return (
    <div>
      <HeadBand
        url={
          "https://roma-montargis.com/wp-content/uploads/2021/03/diapo-menu.jpg?id=12710"
        }
        firstText={""}
        secondText={"LES MENUS"}
        insetX="inset-x-0 top-[300px]"
      />
      <div className="flex">
        <LeftSide
          url={
            "https://roma-montargis.com/wp-content/uploads/2023/06/menu-300x177.jpg"
          }
          title={"Découvrez nos Menus"}
          span={"Les menus du restaurant"}
          height={""}
        />
        <RightSide
          images={[
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/menus-2.jpg",
              heigth: "400px",
            },
          ]}
        />
      </div>
    </div>
  );
}
