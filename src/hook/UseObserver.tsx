type Actions = {
  action: React.Dispatch<React.SetStateAction<string>>;
  param: string;
};

export default function UseObserver(
  ref: React.RefObject<HTMLDivElement>,
  actions: Actions[]
) {
  const observer = new IntersectionObserver((entries) => {
    if (entries[0].isIntersecting) {
      actions.map((action) => action.action(action.param));
    }
  });

  if (ref.current) {
    observer.observe(ref.current);
  }
}
