export default function Logo() {
  return (
    <div className="h-full w-[520px] mr-[145px] flex items-center justify-center">
      <img
        src="https://roma-montargis.com/wp-content/uploads/2021/02/logo-roma-1.gif"
        alt=""
        className="w-[140px] h-[110px]"
      ></img>
    </div>
  );
}
