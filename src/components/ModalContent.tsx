import Nav from "./Nav";

type Props = {
  close: () => void;
};

export default function ModalContent({ close }: Props) {
  return (
    <div
      onClick={close}
      className="absolute flex flex-col top-0 right-0 w-1/2 h-full bg-black/75"
    >
      <Nav textColor="text-white" flexCol="flex-col" />
    </div>
  );
}
