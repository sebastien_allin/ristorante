import { useEffect, useState } from "react";

type Props = {
  url: string;
  firstText: string;
  secondText: string;
  insetX?: string;
};

export default function HeadBand({
  url,
  firstText,
  secondText,
  insetX = "inset-x-0",
}: Props) {
  const [transitionH3, setTransitionH3] = useState("");
  const [transitionH2, setTransitionH2] = useState("");
  const [opacity, setOpacity] = useState("opacity-0");

  useEffect(() => {
    setTransitionH3("translate-y-8");
    setTransitionH2("-translate-y-3");
    setOpacity("opacity-100");
  }, []);
  return (
    <div
      className="w-full text-center h-[400px] pb-[120px] pt-[200px] bg-right md:bg-center bg-no-repeat "
      style={{
        backgroundImage: `url(${url})`,
      }}
    >
      <h2
        className={`text-white text-5xl  font-vibe absolute inset-x-1  ${opacity} transition delay-800 duration-700 ${transitionH2}`}
      >
        {firstText}
      </h2>
      <h3
        className={` text-stone-100 font-bold absolute ${insetX} text-5xl md:text-6xl font-taviraj ${opacity} transition duration-700 delay-800 ${transitionH3}`}
      >
        {secondText}
      </h3>
    </div>
  );
}
