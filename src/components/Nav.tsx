import { NavLink, useLocation } from "react-router-dom";

type NavProps = {
  textColor: string;
  flexCol?: string;
};

export default function Nav({ textColor, flexCol }: NavProps) {
  const location = useLocation();
  return (
    <nav
      className={`flex ${flexCol} ${location} items-center justify-center gap-2 lg:gap-8  flex-wrap w-full`}
    >
      <NavLink
        to={"/"}
        className={`${
          location.pathname === "/" ? "text-blue-500" : textColor
        } link`}
      >
        ACCUEIL
      </NavLink>
      <NavLink
        to={"/pizzas"}
        className={` ${
          location.pathname === "/pizzas" ? "text-blue-500" : textColor
        } link`}
      >
        PIZZAS
      </NavLink>
      <NavLink
        to={"/carte"}
        className={`${
          location.pathname === "/carte" ? "text-blue-500" : textColor
        } link`}
      >
        LA CARTE
      </NavLink>
      <NavLink
        to={"/dessert"}
        className={`${
          location.pathname === "/dessert" ? "text-blue-500" : textColor
        } link`}
      >
        DESSERTS
      </NavLink>
      <NavLink
        to={"/boissons"}
        className={`${
          location.pathname === "/boissons" ? "text-blue-500" : textColor
        } link`}
      >
        BOISSONS
      </NavLink>
      <NavLink
        to={"/menu"}
        className={`${
          location.pathname === "/menu" ? "text-blue-500" : textColor
        } link`}
      >
        MENU
      </NavLink>
      <NavLink
        to={"/contact"}
        className={`${
          location.pathname === "/contact" ? "text-blue-500" : textColor
        } link`}
      >
        CONTACT
      </NavLink>
    </nav>
  );
}
