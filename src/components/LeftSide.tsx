import { useEffect, useState } from "react";

type Props = {
  url: string;
  title: string;
  span: string;
  height: string;
  image?: { url: string; height: string; width: string };
};

export default function LeftSide({
  url,
  title,
  span,
  height = "h-[1400px]",
  image,
}: Props) {
  const [opacity, setOpacity] = useState("opacity-0");
  const [translate, setTranslate] = useState("");

  useEffect(() => {
    setOpacity("opacity-100");
    setTranslate("translate-y-[40px]");
  }, []);

  return (
    <div className="relative ml-1 md:ml-[190px] mt-[100px]">
      <h2
        className={`absolute -inset-y-[100px] text-md md:text-5xl mb-6 duration-1000 delay-1000 ${opacity} ${translate}`}
      >
        {title}
      </h2>
      <span>{span} </span>
      <img className={`mt-6 mb-6 w-[700px] ${height}`} src={url} alt="" />
      {image && (
        <img src={image.url} alt="" width={image.width} height={image.height} />
      )}
    </div>
  );
}
