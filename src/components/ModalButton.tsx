import { useState } from "react";
import { createPortal } from "react-dom";
import ModalContent from "./ModalContent";

export default function ModalButton() {
  const [showModal, setShowModal] = useState(false);
  console.log(showModal);

  return (
    <>
      <div
        onClick={() => setShowModal(true)}
        className={`w-[100px] h-[60px] absolute top-0 right-1 text-center pt-2 border border-black ${
          showModal && "hidden"
        } `}
      >
        Menu
      </div>
      {showModal &&
        createPortal(
          <ModalContent close={() => setShowModal(false)} />,
          document.body
        )}
    </>
  );
}
