type Image = {
  url: string;
  heigth: string;
};

type Props = {
  images: Image[];
};

export default function RightSide({ images }: Props) {
  return (
    <div className="flex flex-col gap-5 ml-0 md:ml-6 mt-[100px] mb-5">
      {images.map((image) => (
        <img
          key={image.url}
          src={image.url}
          width={"360px"}
          height={image.heigth}
          alt=""
        />
      ))}
    </div>
  );
}
