import Nav from "./components/Nav";

export default function Footer() {
  const up = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  return (
    <div className=" bottom-0 bg-[#1a1d29] flex  items-center justify-center w-full h-[230px]">
      <Nav textColor="text-white" />
      <button
        onClick={() => up()}
        className="w-[50px] h-[50px] text-white rounded-full mr-2 border border-white"
      >
        up
      </button>
    </div>
  );
}
