import { useEffect, useRef, useState } from "react";
import HeadBand from "../components/HeadBand";
import Form from "./Form";
import VisitCard from "./VisitCard";
import UseObserver from "../hook/UseObserver";

export default function Contact() {
  const ref = useRef<HTMLInputElement>(null);

  const [opacity, setOpacity] = useState("opacity-0");
  const [translateVisitCard, setTranslateVisitCard] = useState("");
  const [translateForm, setTranslateForm] = useState("");

  useEffect(() => {
    const actions = [
      { action: setOpacity, param: "opacity-100" },
      { action: setTranslateVisitCard, param: "translate-x-[200px]" },
      { action: setTranslateForm, param: "-translate-x-[700px]" },
    ];
    UseObserver(ref, actions);
  }, []);

  return (
    <div>
      <HeadBand
        url={
          "https://roma-montargis.com/wp-content/uploads/2021/03/carte-roma-restaurant-montargis.jpg?id=12745"
        }
        firstText={""}
        secondText={"NOUS CONTACTER"}
      />
      <div className="text-center">
        <p className="text-red-600 text-4xl mt-4">PENSEZ A RESERVER</p>
        <span className="text-red-600 text-2xl">ouverture à partir de 19h</span>
      </div>
      <div className="w-full h-[800px] md:h-[650px] relative">
        <VisitCard opacity={opacity} translate={translateVisitCard} />
        <Form refObs={ref} opacity={opacity} translate={translateForm} />
      </div>
    </div>
  );
}
