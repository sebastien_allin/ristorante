type Props = {
  refObs: React.RefObject<HTMLInputElement>;
  opacity: string;
  translate: string;
};

export default function Form({ refObs, opacity, translate }: Props) {
  return (
    <div
      className={`absolute w-full duration-700 delay-1000 ${opacity} ${translate} inset-x-[710px] md:inset-x-full inset-y-[400px] md:inset-y-[50px]`}
    >
      <h2 className="text-3xl mb-5 ">Nous Contacter</h2>
      <form action="">
        <div className="flex mb-2">
          <input
            ref={refObs}
            type="text"
            className="form-input mr-2 inputWidth"
            placeholder="Nom"
          />
          <input
            type="text"
            className="form-input inputWidth"
            placeholder="E-mail"
          />
        </div>
        <div className="flex mb-2">
          <input
            type="text"
            className="form-input mr-2 inputWidth"
            placeholder="Téléphone"
          />
          <input
            type="text"
            className="form-input inputWidth"
            placeholder="Sujet"
          />
        </div>
        <div>
          <textarea
            placeholder="Message"
            className="form-textarea w-[350px] md:w-[630px] h-[150px]"
          />
        </div>
        <button className="w-[100px] h-10 border border-black">Envoyer</button>
      </form>
    </div>
  );
}
