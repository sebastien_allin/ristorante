type Props = {
  opacity: string;
  translate: string;
};

export default function VisitCard({ opacity, translate }: Props) {
  return (
    <div
      className={`absolute duration-700 delay-1000 ${opacity} ${translate} -inset-x-[190px] md:-inset-x-[150px] inset-y-[50px]`}
    >
      <h2 className="font-vibe text-3xl ">Restaurant Roma - Montargis</h2>
      <p>45200 MONTARGIS</p>
      <span>tel : 06 16 46 15 70 </span>
      <img
        className="mt-10 md:w-[700px] md:h-[380px] "
        src="https://roma-montargis.com/wp-content/uploads/2021/02/la-roma-montargis-300x165.jpg"
        alt=""
      />
    </div>
  );
}
