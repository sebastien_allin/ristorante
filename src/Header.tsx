import Logo from "./components/Logo";
import ModalButton from "./components/ModalButton";
import Nav from "./components/Nav";

export default function Header() {
  return (
    <>
      {window.innerWidth < 1200 ? (
        <div className="flex ">
          <Logo />
          <ModalButton />
        </div>
      ) : (
        <div className="w-full  h-[160px] flex">
          <Logo />
          <Nav textColor="text-black" />
        </div>
      )}
    </>
  );
}
