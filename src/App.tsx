import Footer from "./Footer";
import Header from "./Header";
import { Route, Routes } from "react-router-dom";
import Home from "./home/Home";
import Pizzas from "./pizzas/Pizzas";
import Carte from "./carte/Carte";
import Desserts from "./dessert/Desserts";
import Boissons from "./boissons/Boissons";
import Menu from "./menu/Menu";
import Contact from "./contact/Contact";

function App() {
  // eslint-disable-next-line prefer-const
  return (
    <>
      <div className="max-w-[1600px] m-auto w-full h-screen bg-white">
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/pizzas" element={<Pizzas />} />
          <Route path="/carte" element={<Carte />} />
          <Route path="/dessert" element={<Desserts />} />
          <Route path="/boissons" element={<Boissons />} />
          <Route path="/menu" element={<Menu />} />
          <Route path="/contact" element={<Contact />} />
        </Routes>
        <Footer />
      </div>
    </>
  );
}

export default App;
