import HeadBand from "../components/HeadBand";
import LeftSide from "../components/LeftSide";
import RightSide from "../components/RightSide";

export default function Pizzas() {
  return (
    <div>
      <HeadBand
        url="https://roma-montargis.com/wp-content/uploads/2021/03/pizzas-slide.jpg?id=12708"
        firstText=""
        secondText="PIZZAS"
      />
      <div className="flex">
        <LeftSide
          url={"https://roma-montargis.com/wp-content/uploads/2023/06/roma.jpg"}
          title={"Nos Pizzas"}
          span={"Découvrez nos pizzas"}
          height=""
        />
        <RightSide
          images={[
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/03/pizza-montargis-45-193x300.png",
              heigth: "560px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/pizzeria-roma-300x262.jpg",
              heigth: "315px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/06/salle-resto-italien-roma-montargis.jpg",
              heigth: "315px",
            },
          ]}
        />
      </div>
    </div>
  );
}
