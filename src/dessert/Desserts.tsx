import HeadBand from "../components/HeadBand";
import LeftSide from "../components/LeftSide";
import RightSide from "../components/RightSide";

export default function Desserts() {
  return (
    <div>
      <HeadBand
        url={
          "https://roma-montargis.com/wp-content/uploads/2021/03/tiramisu2.jpg?id=12706"
        }
        firstText={""}
        secondText={"LES DESSERTS"}
      />
      <div className="flex">
        <LeftSide
          url={
            "https://roma-montargis.com/wp-content/uploads/2023/06/desserts-1.jpg"
          }
          title={"Découvrez Nos Desserts"}
          span={"Les desserts du restaurant"}
          height="h-[520px]"
          image={{
            url: "https://roma-montargis.com/wp-content/uploads/2021/06/salle-resto-roma.jpg",
            width: "750px",
            height: "420px",
          }}
        />
        <RightSide
          images={[
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/DESSERT-ROMA-212x300.jpg",
              heigth: "510px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/CAFE-212x300.jpg",
              heigth: "510px",
            },
          ]}
        />
      </div>
    </div>
  );
}
