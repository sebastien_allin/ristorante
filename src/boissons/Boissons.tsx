import HeadBand from "../components/HeadBand";
import LeftSide from "../components/LeftSide";
import RightSide from "../components/RightSide";

export default function Boissons() {
  return (
    <div>
      <HeadBand
        url={
          "https://roma-montargis.com/wp-content/uploads/2021/03/diap-boissons.jpg?id=12716"
        }
        firstText={""}
        secondText={"LES BOISSONS"}
      />
      <div className="flex">
        <LeftSide
          url={
            "https://roma-montargis.com/wp-content/uploads/2023/06/apero-1.jpg"
          }
          title={"Découvrez Nos Boissons"}
          span={"Les boissons du reataurant"}
          height={""}
          image={{
            url: "https://roma-montargis.com/wp-content/uploads/2023/06/apero-2.jpg",
            height: "1400px",
            width: "715px",
          }}
        />
        <RightSide
          images={[
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/boisson-6-212x300.jpg",
              heigth: "512px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/boisson-3-212x300.jpg",
              heigth: "512px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/boisson-4.jpg",
              heigth: "512px",
            },
            {
              url: "https://roma-montargis.com/wp-content/uploads/2021/02/boisson-1-212x300.jpg",
              heigth: "512px",
            },
          ]}
        />
      </div>
    </div>
  );
}
