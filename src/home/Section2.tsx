import { useEffect, useRef, useState } from "react";
import UseObserver from "../hook/UseObserver";

export default function Section2() {
  const [opacity, setOpacity] = useState("opacity-0");
  const [transitionP, setTransitionP] = useState("");
  const [transitionP2, setTransitionP2] = useState("");
  const [transitionH2, setTransitionH2] = useState("");

  const Ref = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    const actions = [
      { action: setOpacity, param: "opacity-100" },
      { action: setTransitionP, param: "translate-y-3" },
      { action: setTransitionP2, param: "translate-y-10" },
      { action: setTransitionH2, param: "-translate-y-4" },
    ];
    UseObserver(Ref, actions);
  }, []);

  return (
    <div className="relative mt-[50px] md:mt-[150px] leading-loose w-full flex flex-col items-center  h-[210px]">
      <h2
        className={`absolute transition duration-700 delay-800  text-xl md:text-3xl font-vibe ${opacity} ${transitionH2}`}
      >
        Roma - Restaurant Italien à Montargis
      </h2>
      <p
        className={`absolute font-bold transition duration-700 delay-800 text-xl  md:text-3xl ${opacity} ${transitionP}`}
      >
        Le restaurant Roma vous accueil
      </p>
      <p
        className={`absolute font-bold transition duration-700 delay-800 text-xl  md:text-3xl ${opacity} ${transitionP2}`}
      >
        dans la plus pure tradition italienne
      </p>

      <span ref={Ref} className="absolute font-medium inset-y-[90px]">
        SPECIALITES ITALIENNES - PIZZAS
      </span>
    </div>
  );
}
