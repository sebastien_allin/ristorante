import CardVsCard from "./CardVsCard";
import Paragraphe from "./Paragraphe";

export default function Section5() {
  return (
    <>
      {window.innerWidth < 500 ? (
        <div className="flex flex-col">
          <Paragraphe />
        </div>
      ) : (
        <div className="relative w-full h-[600px] border flex flex-col md:flex-row">
          <CardVsCard />
          <Paragraphe />
        </div>
      )}
    </>
  );
}
