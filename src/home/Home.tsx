import HeadBand from "../components/HeadBand";
import Section2 from "./Section2";
import Section3 from "./Section3";
import Section4 from "./Section4";
import Section5 from "./Section5";

export default function Home() {
  return (
    <div className="w-full">
      <HeadBand
        url="https://roma-montargis.com/wp-content/uploads/2021/03/roma-restaurant-montargis-slide-1-1.jpg?id=12737"
        firstText="Bienvenue"
        secondText="Ristorente Italiano"
      />
      <Section2 />
      <Section3 />
      <Section4 />
      <Section5 />
    </div>
  );
}
