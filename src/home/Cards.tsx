type CardsProps = {
  url: string;
  title: string;
};

export default function Cards({ url, title }: CardsProps) {
  return (
    <div className="relative h-[520px] w-[360px] overflow-hidden">
      <img
        src={url}
        alt=""
        className=" w-full h-full hover:scale-110 transition duration-700 delay-5000 "
      />
      <div className="bg-white w-[295px] h-[60px] font-bold absolute inset-y-1/2 inset-x-6 flex items-center justify-center ">
        {title}
      </div>
    </div>
  );
}
