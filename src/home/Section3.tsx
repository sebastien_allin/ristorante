import Cards from "./Cards";

export default function Section3() {
  return (
    <div
      className={`w-full flex flex-col md:flex-row items-center  justify-center gap-8 px-[50px]`}
    >
      <Cards
        url="https://roma-montargis.com/wp-content/uploads/2021/02/pasta.jpg"
        title="VENTE A EMPORTER"
      />
      <Cards
        url="https://roma-montargis.com/wp-content/uploads/2021/02/pizza.jpg"
        title="VENTE A LIVRER"
      />
      <Cards
        url="https://roma-montargis.com/wp-content/uploads/2021/02/vente-emporter-roma2.jpg"
        title="BOUTIQUE"
      />
    </div>
  );
}
