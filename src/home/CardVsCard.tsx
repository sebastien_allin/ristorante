import { useEffect, useRef, useState } from "react";
import UseObserver from "../hook/UseObserver";

export default function CardVsCard() {
  const [topUp, setTopUp] = useState("");
  const [topDown, setTopDown] = useState("");
  const [opacity, setOpacity] = useState("opacity-0");

  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (window.innerWidth < 500) {
      return;
    }
    const actions = [
      { action: setTopUp, param: "translate-y-[100px]" },
      { action: setTopDown, param: "-translate-y-[250px]" },
      { action: setOpacity, param: "opacity-100" },
    ];
    UseObserver(ref, actions);
  }, []);

  return (
    <div className="relative flex-1 ">
      <img
        className={`absolute top-[300px] w-[340px] h-[540] duration-700 delay-500  md:inset-x-[190px] ${opacity} ${topDown}`}
        src="https://roma-montargis.com/wp-content/uploads/2021/02/our-history-1.png"
        alt=""
      />
      <img
        className={`absolute top-0 w-[340px] h-[540] duration-700 delay-500 md:inset-x-[350px] ${opacity} ${topUp}`}
        src="https://roma-montargis.com/wp-content/uploads/2021/03/our-history-2.png"
        alt=""
      />
      <div ref={ref} className="absolute top-1/3"></div>
    </div>
  );
}
