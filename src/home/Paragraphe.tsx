import { useEffect, useRef, useState } from "react";
import UseObserver from "../hook/UseObserver";

export default function Paragraphe() {
  const [opacity, setOpacity] = useState("opacity-0");
  const [translateH2, setTranslateH2] = useState("");
  const [translateSpan, setTranslateSpan] = useState("");

  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const actions = [
      { action: setTranslateH2, param: "-translate-y-[5px]" },
      { action: setTranslateSpan, param: "translate-y-[30px]" },
      { action: setOpacity, param: "opacity-100" },
    ];
    UseObserver(ref, actions);
  }, []);

  return (
    <div className="flex-1 ml-2 mt-4">
      <div className="relative h-[80px] w-full md:w-[500px]">
        <h2
          className={`absolute font-vibe duration-700 delay-500 text-2xl ${opacity} ${translateH2}`}
        >
          Depuis 2003
        </h2>
        <span
          className={`absolute text-3xl md:text-4xl duration-700 delay-500 font-bold ${opacity} ${translateSpan}`}
        >
          Le Restaurant Roma
        </span>
      </div>
      <div ref={ref}></div>
      <p className="w-full md:w-[500px] mt-4 leading-relaxed  text-base">
        Fondée en 2003 par Arduino Martini et son fils, le restaurant Roma vous
        accueille dans la plus pure tradition italienne. Au coeur de Montargis,
        ce « petit coin » d’Italie propose une cuisine créative dans une
        ambiance chaleureuse et conviviale avec en plus une terrasse « Lounge »
        repensée dernièrement.
      </p>
      <p className="w-full md:w-[535px] mt-4 leading-relaxed text-base">
        On citera les antipasti fait maison avec des produits frais, ainsi que
        les desserts également fait maison. La pâte à pizza est de fabrication
        maison selon la tradition napolitaine. Tous nos produits proviennent
        directement d’Italie, charcuterie, fromages, pâtes, vins, etc. via la
        maison CARNIATO.
      </p>
      <p className="w-full md:w-[535px] mt-4 leading-relaxed  text-base">
        Nous concevons votre menu de groupe sur demande. N’hésitez pas à nous
        contacter.
      </p>
    </div>
  );
}
