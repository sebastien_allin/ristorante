import { useRef, useState } from "react";
import UseObserver from "../hook/UseObserver";
import { useEffect } from "react";

type Actions = {
  action: React.Dispatch<React.SetStateAction<string>>;
  param: string;
};

export default function Section4() {
  const ref = useRef<HTMLDivElement>(null);

  const [opacity, setOpacity] = useState("opacity-0");
  const [insetH2, setInsetH2] = useState("");
  const [insetP1, setInsetP1] = useState("");
  const [insetP2, setInsetP2] = useState("");

  useEffect(() => {
    const action: Actions[] = [
      { action: setOpacity, param: "opacity-100" },
      { action: setInsetH2, param: "translate-y-40" },
      { action: setInsetP1, param: "translate-y-[50px]" },
      { action: setInsetP2, param: "translate-y-[80px]" },
    ];
    UseObserver(ref, action);
  }, []);

  return (
    <div
      className="relative  mt-[146px] w-full h-[460px] bg-cover overflow-hidden "
      style={{
        backgroundImage:
          "url(https://roma-montargis.com/wp-content/uploads/2021/02/salle-restaurant-roma-montargis.jpg?id=12581) ",
      }}
    >
      <h2
        className={`text-white w-full text-3xl md:text-5xl duration-700 delay-500 font-vibe absolute inset-x-[20%] md:inset-x-[40%] top-0 ${insetH2} ${opacity} `}
      >
        Ristorente - Pizzeria
      </h2>
      <p
        className={`text-white text-xl md:text-3xl duration-700 delay-1000 absolute inset-x-[10%] md:inset-x-[28%]  w-full top-[35%] ${insetP1} ${opacity}`}
      >
        Au coeur de Montargis, ce "petit coin " d'Italie propose
      </p>
      <p
        className={`text-white text-xl md:text-3xl duration-700 delay-1000 absolute inset-x-[10%] md:inset-x-[30%] mt-[50%] md:top-[35%]  w-full ${insetP2} ${opacity}`}
      >
        une cuisine créative dans ambiance chaleureuse
      </p>
      <div ref={ref}></div>
    </div>
  );
}
